package constant;

/**
 * @ClassName MenuLevel
 * @Author zrx
 * @Date 2022/3/18 16:04
 */
public enum MenuLevel {
	/**
	 * 父级，子级
	 */
	PARENT,CHILD
}
