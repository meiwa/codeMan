package entity;

import entity.cmnSys.CmSysMenu;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Parameters implements Serializable {

	private static final long serialVersionUID = -6803800574893433206L;

	private String projectNameVal;

	private String proCnNameVal;

	private String frameWorkVal = "springBoot";

	private String clientFrameWorkVal = "custom-经典";

	/**
	 * 数据源名称
	 */
	private String dataSourceName;

	/**
	 * 是否开启多数据源
	 */
	private boolean isMutiDataSource;

	private String dataBaseTypeVal = "";

	private String dataBaseIpVal;

	private String dataBasePortVal;

	private String dataBasePwdVal;

	private String dataBaseNameVal;

	private String dataBaseUserNameVal;

	private String dataBaseUrl;

	private String dataBaseDriverClass;

	private String outPathVal;

	private String makeModelVal = "静态用户";

	private String tableName;

	private String jsFrameWork = "vue";

	private String themeVal = "前后端分离响应式";

	/**
	 * 是否需要权限管理
	 */
	private boolean isAuthority;

	//2021-11-23
	private boolean isController;
	private boolean isService;
	private boolean isDao;
	private boolean isEntity;
	private boolean isView;

	//2022-03-16
	private boolean cloudModel = false;
	private String cloudRegiseterCenter = "eureka";
	private String cloudNeteWork = "gateway";
	private String cloudSysName = "cloud";
	private String cloudSysEngName = "cloud";
	//cloud模式下的所有menu
	private List<CmSysMenu> cloudMenus = new ArrayList<>();
	//cloud模式下的所有微服务
	private List<String> cloudServices = new ArrayList<>();
	//仅生成微服务模块
	private boolean onlyCloudModel = false;
	private boolean redisSingleLogin = false;
	private boolean coverExitFile = true;
	/**
	 * cloud系统服务数据源名称
	 */
	private Parameters cloudSysDsParam;

	public Parameters() {
		super();
	}

	public String getProjectNameVal() {
		return projectNameVal;
	}

	public void setProjectNameVal(String projectNameVal) {
		this.projectNameVal = projectNameVal;
	}

	public String getProCnNameVal() {
		return proCnNameVal;
	}

	public void setProCnNameVal(String proCnNameVal) {
		this.proCnNameVal = proCnNameVal;
	}

	public String getFrameWorkVal() {
		return frameWorkVal;
	}

	public void setFrameWorkVal(String frameWorkVal) {
		this.frameWorkVal = frameWorkVal;
	}

	public String getClientFrameWorkVal() {
		return clientFrameWorkVal;
	}

	public void setClientFrameWorkVal(String clientFrameWorkVal) {
		this.clientFrameWorkVal = clientFrameWorkVal;
	}

	public String getDataSourceName() {
		return dataSourceName;
	}

	public void setDataSourceName(String dataSourceName) {
		this.dataSourceName = dataSourceName;
	}

	public boolean isMutiDataSource() {
		return isMutiDataSource;
	}

	public void setMutiDataSource(boolean mutiDataSource) {
		isMutiDataSource = mutiDataSource;
	}

	public String getDataBaseTypeVal() {
		return dataBaseTypeVal;
	}

	public void setDataBaseTypeVal(String dataBaseTypeVal) {
		this.dataBaseTypeVal = dataBaseTypeVal;
	}

	public String getDataBaseIpVal() {
		return dataBaseIpVal;
	}

	public void setDataBaseIpVal(String dataBaseIpVal) {
		this.dataBaseIpVal = dataBaseIpVal;
	}

	public String getDataBasePortVal() {
		return dataBasePortVal;
	}

	public void setDataBasePortVal(String dataBasePortVal) {
		this.dataBasePortVal = dataBasePortVal;
	}

	public String getDataBasePwdVal() {
		return dataBasePwdVal;
	}

	public void setDataBasePwdVal(String dataBasePwdVal) {
		this.dataBasePwdVal = dataBasePwdVal;
	}

	public String getDataBaseNameVal() {
		return dataBaseNameVal;
	}

	public void setDataBaseNameVal(String dataBaseNameVal) {
		this.dataBaseNameVal = dataBaseNameVal;
	}

	public String getDataBaseUserNameVal() {
		return dataBaseUserNameVal;
	}

	public void setDataBaseUserNameVal(String dataBaseUserNameVal) {
		this.dataBaseUserNameVal = dataBaseUserNameVal;
	}

	public String getOutPathVal() {
		return outPathVal;
	}

	public void setOutPathVal(String outPathVal) {
		this.outPathVal = outPathVal;
	}

	public String getMakeModelVal() {
		return makeModelVal;
	}

	public void setMakeModelVal(String makeModelVal) {
		this.makeModelVal = makeModelVal;
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public String getDataBaseUrl() {
		return dataBaseUrl;
	}

	public void setDataBaseUrl(String dataBaseUrl) {
		this.dataBaseUrl = dataBaseUrl;
	}

	public String getDataBaseDriverClass() {
		return dataBaseDriverClass;
	}

	public void setDataBaseDriverClass(String dataBaseDriverClass) {
		this.dataBaseDriverClass = dataBaseDriverClass;
	}

	public String getJsFrameWork() {
		return jsFrameWork;
	}

	public void setJsFrameWork(String jsFrameWork) {
		this.jsFrameWork = jsFrameWork;
	}

	public String getThemeVal() {
		return themeVal;
	}

	public void setThemeVal(String themeVal) {
		this.themeVal = themeVal;
	}

	public boolean isAuthority() {
		return isAuthority;
	}

	public void setAuthority(boolean authority) {
		isAuthority = authority;
	}

	public boolean isController() {
		return isController;
	}

	public void setController(boolean controller) {
		isController = controller;
	}

	public boolean isService() {
		return isService;
	}

	public void setService(boolean service) {
		isService = service;
	}

	public boolean isDao() {
		return isDao;
	}

	public void setDao(boolean dao) {
		isDao = dao;
	}

	public boolean isEntity() {
		return isEntity;
	}

	public void setEntity(boolean entity) {
		isEntity = entity;
	}

	public boolean isView() {
		return isView;
	}

	public void setView(boolean view) {
		isView = view;
	}

	public boolean isCloudModel() {
		return cloudModel;
	}

	public void setCloudModel(boolean cloudModel) {
		this.cloudModel = cloudModel;
	}

	public String getCloudRegiseterCenter() {
		return cloudRegiseterCenter;
	}

	public void setCloudRegiseterCenter(String cloudRegiseterCenter) {
		this.cloudRegiseterCenter = cloudRegiseterCenter;
	}

	public String getCloudNeteWork() {
		return cloudNeteWork;
	}

	public void setCloudNeteWork(String cloudNeteWork) {
		this.cloudNeteWork = cloudNeteWork;
	}

	public List<CmSysMenu> getCloudMenus() {
		return cloudMenus;
	}

	public void setCloudMenus(List<CmSysMenu> cloudMenus) {
		this.cloudMenus = cloudMenus;
	}

	public List<String> getCloudServices() {
		return cloudServices;
	}

	public void setCloudServices(List<String> cloudServices) {
		this.cloudServices = cloudServices;
	}

	public boolean isOnlyCloudModel() {
		return onlyCloudModel;
	}

	public void setOnlyCloudModel(boolean onlyCloudModel) {
		this.onlyCloudModel = onlyCloudModel;
	}

	public String getCloudSysName() {
		return cloudSysName;
	}

	public void setCloudSysName(String cloudSysName) {
		this.cloudSysName = cloudSysName;
	}

	public boolean isRedisSingleLogin() {
		return redisSingleLogin;
	}

	public void setRedisSingleLogin(boolean redisSingleLogin) {
		this.redisSingleLogin = redisSingleLogin;
	}

	public boolean isCoverExitFile() {
		return coverExitFile;
	}

	public void setCoverExitFile(boolean coverExitFile) {
		this.coverExitFile = coverExitFile;
	}

	public String getCloudSysEngName() {
		return cloudSysEngName;
	}

	public void setCloudSysEngName(String cloudSysEngName) {
		this.cloudSysEngName = cloudSysEngName;
	}

	public Parameters getCloudSysDsParam() {
		return cloudSysDsParam;
	}

	public void setCloudSysDsParam(Parameters cloudSysDsParam) {
		this.cloudSysDsParam = cloudSysDsParam;
	}
}
