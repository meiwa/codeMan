package util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.swing.JOptionPane;

import constant.Constant;
import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.exception.ZipException;

public class ZipUtils {

    // 删除所有文件
    public static void delDir(File file) {
        if (!file.exists()) {
            return;
        }
        if (file.isDirectory()) {
            File zFiles[] = file.listFiles();
            for (File file2 : zFiles) {
                delDir(file2);
            }
            file.delete();
        } else {
            file.delete();
        }
    }

    // 解压加密的压缩文件
    public static void unZip(File zipFile, String dest, String pwd) {

        try {

            ZipFile zFile = new ZipFile(zipFile); // 首先创建ZipFile指向磁盘上的.zip文件
            zFile.setFileNameCharset("GBK"); // 设置文件名编码，在GBK系统中需要设置
            if (!zFile.isValidZipFile()) { // 验证.zip文件是否合法，包括文件是否存在、是否为zip文件、是否被损坏等
                JOptionPane.showMessageDialog(Constant.frmv, "压缩文件不合法,可能被损坏.", "错误", JOptionPane.ERROR_MESSAGE);
            }
            File destDir = new File(dest); // 解压目录
            if (destDir.isDirectory() && !destDir.exists()) {
                destDir.mkdir();
            }
            if (zFile.isEncrypted()) {
                zFile.setPassword(pwd.toCharArray()); // 设置密码
            }
            zFile.extractAll(dest); // 将文件抽出到解压目录(解压)
        } catch (ZipException e) {
            JOptionPane.showMessageDialog(Constant.frmv, "解析压缩文件过程中出现错误！", "错误", JOptionPane.ERROR_MESSAGE);
        }

    }

    // 删除解压后的模板文件
    public static void deleteFiles(String path) {
        File filePath = new File(path);

        File[] listFiles = filePath.listFiles();

        for (File file : listFiles) {

            if (!file.getName().equals("model.zip")) {
                delDir(file);
            }

        }
    }

    /**
     * 复制文件夹
     *
     * @param oldPath
     * @param newPath
     * @throws IOException
     */
    public static void copyDir(String oldPath, String newPath) throws IOException {
        File file = new File(oldPath);
        // 文件名称列表
        String[] filePath = file.list();

        if (!(new File(newPath)).exists()) {
            (new File(newPath)).mkdir();
        }

        for (int i = 0; i < filePath.length; i++) {
            if ((new File(oldPath + File.separator + filePath[i])).isDirectory()) {
                copyDir(oldPath + File.separator + filePath[i], newPath + File.separator + filePath[i]);
            }

            if (new File(oldPath + File.separator + filePath[i]).isFile()) {
                copyFile(oldPath + File.separator + filePath[i], newPath + File.separator + filePath[i]);
            }

        }
    }

    /**
     * 复制文件
     *
     * @param oldPath
     * @param newPath
     * @throws IOException
     */
    public static void copyFile(String oldPath, String newPath) throws IOException {

        FileInputStream in = null;
        FileOutputStream out = null;
        try {
            File oldFile = new File(oldPath);
            File file = new File(newPath);

            in = new FileInputStream(oldFile);
            out = new FileOutputStream(file);

            byte[] buffer = new byte[2048];

            while ((in.read(buffer)) != -1) {
                out.write(buffer);
            }
            out.flush();
        } finally {
            if (in != null) {
                in.close();
            }
            if (out != null) {
                out.close();
            }
        }
    }

}
