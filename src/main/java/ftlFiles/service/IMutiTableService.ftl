package <#if isCloudModel>${cloudSysEngName}<#else>${packageName}</#if>.service;

<#list currentMethodMap?keys as key>
import <#if isCloudModel>${cloudSysEngName}<#else>${packageName}</#if>.entity.${currentMethodMap["${key}"].entityName?cap_first}Muti;
</#list>
import <#if isCloudModel>${cloudSysEngName}<#else>${packageName}</#if>.entity.PageData;
import javax.servlet.http.HttpServletResponse;

public interface ${capCurrentMutiEng}MutiService {

	<#list currentMethodMap?keys as key>
	<#assign entityName = currentMethodMap["${key}"].entityName/>
	/**
	 * ${key}
	 *
	 */
	PageData<${entityName?cap_first}Muti> ${key}(${entityName?cap_first}Muti entity);

	/**
	 * 导出excel
	 *
	 */
	void ${key}ExportExcel(${entityName?cap_first}Muti entity, HttpServletResponse response);

	</#list>



}
