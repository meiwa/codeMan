package <#if isCloudModel>${cloudSysEngName}<#else>${packageName}</#if>.service;

import <#if isCloudModel>${cloudSysEngName}<#else>${packageName}</#if>.entity.User;

public interface LoginService {

	User login(User user);

}
