package <#if isCloudModel>${cloudSysEngName}<#else>${packageName}</#if>.service;

import javax.servlet.http.HttpServletResponse;

<#if !entityName??>
import java.util.Map;
<#else>
import <#if isCloudModel>${cloudSysEngName}<#else>${packageName}</#if>.entity.${entityName};
</#if>
import <#if isCloudModel>${cloudSysEngName}<#else>${packageName}</#if>.entity.PageData;
import java.util.List;

<#if !entityName??>
public interface ${IServiceName} {

	void add(Map<String, Object> map);

	void delete(Map<String, Object> map);

	void update(Map<String, Object> map);

	List<Map<String,Object>> select(Map<String, Object> map);

	Map<String, Object> likeSelect(Map<String, Object> map);

	void exportExcel(Map<String, Object> paramMap, HttpServletResponse response);

}
<#else>
public interface ${IServiceName} {

	void add(${entityName} entity);

	void delete(${entityName} entity);

	void update(${entityName} entity);

	List<${entityName}> select(${entityName} entity);

	PageData<${entityName}> likeSelect(${entityName} entity);

    void exportExcel(${entityName} entity, HttpServletResponse response);

}
</#if>
