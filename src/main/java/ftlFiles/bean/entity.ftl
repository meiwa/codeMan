package <#if isCloudModel>${cloudSysEngName}<#else>${packageName}</#if>.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
<#if ifUseSwagger == "是">
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
</#if>
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.EqualsAndHashCode;
import java.io.Serializable;
<#list entityNameAndTypes as nameAndTypeAndComment>
<#--如果className不为空-->
<#if nameAndTypeAndComment.className !="">
import ${nameAndTypeAndComment.className};
</#if>
</#list>

<#if ifUseSwagger == "是">
@ApiModel
</#if>
@Data
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class ${entityName} extends CommonEntity implements Serializable {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 1L;

<#list entityNameAndTypes as nameAndTypeAndComment>
    /**
	 *  ${nameAndTypeAndComment.comment}
	 */
	<#if ifUseSwagger == "是">
	@ApiModelProperty(value = "${nameAndTypeAndComment.comment}", name = "${nameAndTypeAndComment.sqlParamName}")
	</#if>
    private ${nameAndTypeAndComment.typeName} ${nameAndTypeAndComment.sqlParamName};
</#list>

<#list queryColumnList as data>
    <#assign javaType = "String">
    <#if data.serviceType == "字符串" || data.serviceType == "文字域" || data.serviceType == "布尔" || data.serviceType == "状态码">
        <#assign javaType = "String">
    <#elseif data.serviceType == "数字">
        <#assign javaType = "Integer">
    <#elseif data.serviceType == "日期">
        <#assign javaType = "java.util.Date">
    </#if>
    <#if data.compareValue==">= && <=">
    /**
	 *  开始${data.columnsCn}
	 */
        <#if ifUseSwagger == "是">
	@ApiModelProperty(value = "开始${data.columnsCn}", name = "start${data.sqlParamColumnEng?cap_first}")
        </#if>
    private ${javaType} start${data.sqlParamColumnEng?cap_first};

    /**
	 *  结束${data.columnsCn}
	 */
        <#if ifUseSwagger == "是">
	@ApiModelProperty(value = "结束${data.columnsCn}", name = "end${data.sqlParamColumnEng?cap_first}")
        </#if>
    private ${javaType} end${data.sqlParamColumnEng?cap_first};
    </#if>
</#list>

}
