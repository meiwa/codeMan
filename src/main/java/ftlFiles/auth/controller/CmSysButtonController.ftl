package <#if isCloudModel>${cloudSysEngName}<#else>${packageName}</#if>.controller;

import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.PostMapping;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import <#if isCloudModel>${cloudSysEngName}<#else>${packageName}</#if>.constant.YesOrNo;
import <#if isCloudModel>${cloudSysEngName}<#else>${packageName}</#if>.entity.CmSysMenuEntity;
import <#if isCloudModel>${cloudSysEngName}<#else>${packageName}</#if>.entity.CmSysUserEntity;
import <#if isCloudModel>${cloudSysEngName}<#else>${packageName}</#if>.service.CmSysButtonService;
import <#if isCloudModel>${cloudSysEngName}<#else>${packageName}</#if>.core.annotation.LoginRequired;
import <#if isCloudModel>${cloudSysEngName}<#else>${packageName}</#if>.core.annotation.RecordLog;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import <#if isCloudModel>${cloudSysEngName}<#else>${packageName}</#if>.entity.CmSysButtonEntity;
import <#if isCloudModel>${cloudSysEngName}<#else>${packageName}</#if>.utils.SessionUtil;
import <#if isCloudModel>${cloudSysEngName}<#else>${packageName}</#if>.utils.SnowflakeIdWorker;

import java.util.Date;
import java.util.List;

@RestController
@Api(tags = "cmSysButton接口")
@RequestMapping("/cmSysButton")
public class CmSysButtonController {


	private final CmSysButtonService service;

	@Autowired
	public CmSysButtonController(CmSysButtonService service) {
		this.service = service;
	}

	/**
	 * 更新
	 *
	 * @return
	 */
	@ApiOperation(value = "更新")
	@LoginRequired
	@RecordLog
	@PostMapping(value = "/update")
	public void update(@RequestBody @Validated CmSysButtonEntity entity, HttpServletRequest request) {
		CmSysUserEntity user = SessionUtil.getUser(request);
		entity.setUpdateUserId(user.getUserId());
		entity.setUpdateTime(new Date());
		service.update(entity);
	}

	/**
	 * 添加
	 *
	 * @return
	 */
	@ApiOperation(value = "添加")
	@LoginRequired
	@RecordLog
	@PostMapping(value = "/add")
	public CmSysMenuEntity add(@RequestBody @Validated CmSysButtonEntity entity, HttpServletRequest request) {
		CmSysUserEntity user = SessionUtil.getUser(request);
		entity.setButtonId(SnowflakeIdWorker.generateId());
		entity.setCreateUserId(user.getUserId());
		entity.setCreateTime(new Date());
		CmSysMenuEntity menuEntity = new CmSysMenuEntity();
		//前端用
		menuEntity.setMenuId(entity.getButtonId());
		menuEntity.setName(entity.getButtonName());
		menuEntity.setParentId(entity.getMenuId());
		menuEntity.setIsButton(YesOrNo.YES.getCode());
		menuEntity.setModuleTagId(entity.getModuleTagId());
		service.add(entity);
		return menuEntity;
	}

	/**
	 * 删除
	 *
	 * @return
	 */
	@ApiOperation(value = "删除")
	@LoginRequired
	@RecordLog
	@PostMapping(value = "/delete")
	public void delete(@RequestBody CmSysButtonEntity entity) {
		service.delete(entity);
	}

}
