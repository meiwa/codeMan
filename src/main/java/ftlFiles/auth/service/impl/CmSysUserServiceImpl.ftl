package <#if isCloudModel>${cloudSysEngName}<#else>${packageName}</#if>.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import <#if isCloudModel>${cloudSysEngName}<#else>${packageName}</#if>.core.exception.BusinessException;
import <#if isCloudModel>${cloudSysEngName}<#else>${packageName}</#if>.dao.CmSysUserDao;
import <#if isCloudModel>${cloudSysEngName}<#else>${packageName}</#if>.entity.CmSysUserEntity;
import <#if isCloudModel>${cloudSysEngName}<#else>${packageName}</#if>.entity.PageData;
import <#if isCloudModel>${cloudSysEngName}<#else>${packageName}</#if>.service.CmSysRoleService;
import <#if isCloudModel>${cloudSysEngName}<#else>${packageName}</#if>.service.CmSysUserService;
import <#if isCloudModel>${cloudSysEngName}<#else>${packageName}</#if>.utils.Md5Util;
import <#if isCloudModel>${cloudSysEngName}<#else>${packageName}</#if>.utils.PageUtil;
import <#if isCloudModel>${cloudSysEngName}<#else>${packageName}</#if>.utils.SessionUtil;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author zrx
 */
@Service
public class CmSysUserServiceImpl implements CmSysUserService {


	private final CmSysUserDao userDao;

	private final CmSysRoleService roleService;

	private static final String ADMIN_USER = "admin";

	@Autowired
	public CmSysUserServiceImpl(CmSysUserDao userDao, CmSysRoleService roleService) {
		this.userDao = userDao;
		this.roleService = roleService;
	}

	@Override
	public void add(CmSysUserEntity entity) {
		CmSysUserEntity userEntity = userDao.getByUserName(entity.getUsername());
		if (userEntity != null) {
			throw new BusinessException("用户名已存在！");
		}
		userDao.add(entity);
	}

	@Override
	public void delete(CmSysUserEntity entity) {
		if (entity.getUserId() == 1) {
			throw new BusinessException("admin用户不允许删除！");
		}
		userDao.delete(entity);
	}

	@Override
	public void update(CmSysUserEntity entity) {
		CmSysUserEntity oldUser = userDao.getById(entity.getUserId());
		CmSysUserEntity userEntity = userDao.getByUserName(entity.getUsername());
		if (userEntity != null && !oldUser.getUsername().equals(userEntity.getUsername())) {
			throw new BusinessException("要修改的用户名已存在！");
		}
		if (ADMIN_USER.equals(oldUser.getUsername())) {
			throw new BusinessException("admin用户不允许修改名称");
		}
		userDao.update(entity);
	}

	@Override
	public List<CmSysUserEntity> select(CmSysUserEntity entity) {
		return userDao.select(entity);
	}


	@Override
	public PageData<CmSysUserEntity> pageRole(String username, Integer currentPage, Integer pageSize) {
		CmSysUserEntity cmSysUserEntity = new CmSysUserEntity();
		cmSysUserEntity.setUsername(username);
		cmSysUserEntity.setCurrentPage(currentPage);
		cmSysUserEntity.setPageSize(pageSize);
		return PageUtil.getPageData(cmSysUserEntity, userDao, "pageRole");
	}

	@Override
	public CmSysUserEntity doLogin(CmSysUserEntity user, HttpServletRequest request) {
		CmSysUserEntity userEntity = userDao.getByUserName(user.getUsername());
		if (userEntity == null) {
			throw new BusinessException("用户名或密码错误！");
		}
		if (Md5Util.digest(user.getPassword()).equals(userEntity.getPassword())) {
			//验证通过
        <#if isRedisSingleLogin>
            SessionUtil.onLogin(userEntity);
        <#else>
            SessionUtil.onLogin(userEntity, request);
        </#if>
		} else {
			throw new BusinessException("用户名或密码错误！");
		}
		userEntity.setPassword(null);
		//获取拥有的菜单
		userEntity.setMenus(roleService.getMenusById(userEntity.getRoleId()));
		userEntity.setButtons(roleService.getButtonsById(userEntity.getRoleId()));
		//获取拥有的按钮
		return userEntity;
	}
}
