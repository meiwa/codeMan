<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd" >
<mapper namespace="<#if isCloudModel>${cloudSysEngName}<#else>${packageName}</#if>.dao.CmSysButtonDao">

    <!--添加-->
	<insert id="add" parameterType="<#if isCloudModel>${cloudSysEngName}<#else>${packageName}</#if>.entity.CmSysButtonEntity">
		insert into cm_sys_button
            <trim prefix="(" suffix=")" suffixOverrides=",">
				<if test="buttonId != null">
					button_id,
				</if>
				<if test="menuId != null">
					menu_id,
				</if>
				<if test="buttonName != null">
					button_name,
				</if>
				<if test="moduleTagId != null">
					module_tag_id,
				</if>
				<if test="createTime != null">
					create_time,
				</if>
				<if test="updateTime != null">
					update_time,
				</if>
				<if test="createUserId != null">
					create_user_id,
				</if>
				<if test="updateUserId != null">
					update_user_id,
				</if>
		</trim>
		<trim prefix="values (" suffix=")" suffixOverrides=",">
				<if test="buttonId != null">
					<#noparse>#{</#noparse>buttonId},
				</if>
				<if test="menuId != null">
					<#noparse>#{</#noparse>menuId},
				</if>
				<if test="buttonName != null">
					<#noparse>#{</#noparse>buttonName},
				</if>
				<if test="moduleTagId != null">
					<#noparse>#{</#noparse>moduleTagId},
				</if>
				<if test="createTime != null">
					<#noparse>#{</#noparse>createTime},
				</if>
				<if test="updateTime != null">
					<#noparse>#{</#noparse>updateTime},
				</if>
				<if test="createUserId != null">
					<#noparse>#{</#noparse>createUserId},
				</if>
				<if test="updateUserId != null">
					<#noparse>#{</#noparse>updateUserId},
				</if>
		</trim>
	</insert>

    <!--删除-->
	<delete id="delete" parameterType="<#if isCloudModel>${cloudSysEngName}<#else>${packageName}</#if>.entity.CmSysButtonEntity">
		delete from cm_sys_button
		<where>
            <if test="buttonId != null">
                and button_id=<#noparse>#{</#noparse>buttonId}
            </if>
            <if test="buttonId == null">
                and 1 = 0
            </if>
		</where>
	</delete>

    <!--更新-->
	<update id="update" parameterType="<#if isCloudModel>${cloudSysEngName}<#else>${packageName}</#if>.entity.CmSysButtonEntity">
		update cm_sys_button
		<trim prefix="set" suffixOverrides=",">
				<if test="buttonId != null">
					button_id=<#noparse>#{</#noparse>buttonId},
				</if>
				<if test="menuId != null">
					menu_id=<#noparse>#{</#noparse>menuId},
				</if>
				<if test="buttonName != null">
					button_name=<#noparse>#{</#noparse>buttonName},
				</if>
				<if test="moduleTagId != null">
					module_tag_id=<#noparse>#{</#noparse>moduleTagId},
				</if>
				<if test="createTime != null">
					create_time=<#noparse>#{</#noparse>createTime},
				</if>
				<if test="updateTime != null">
					update_time=<#noparse>#{</#noparse>updateTime},
				</if>
				<if test="createUserId != null">
					create_user_id=<#noparse>#{</#noparse>createUserId},
				</if>
				<if test="updateUserId != null">
					update_user_id=<#noparse>#{</#noparse>updateUserId},
				</if>
		</trim>
		<where>
            <if test="buttonId != null">
                and button_id=<#noparse>#{</#noparse>buttonId}
            </if>
            <if test="buttonId == null">
                and 1 = 0
            </if>
		</where>
	</update>

</mapper>
