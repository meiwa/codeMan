package <#if isCloudModel>${cloudSysEngName}<#else>${packageName}</#if>.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import <#if isCloudModel>${cloudSysEngName}<#else>${packageName}</#if>.core.annotation.ExternalField;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

@ApiModel
@Data
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class CmSysMenuEntity extends CommonEntity implements Serializable {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * menu_id
	 */
	@ApiModelProperty(value = "menu_id", name = "menuId")
	private Long menuId;
	/**
	 * parent_id
	 */
	@ApiModelProperty(value = "parent_id", name = "parentId")
	@NotNull(message = "父类id不能为空")
	private Long parentId;

	/**
	 * order_no
	 */
	@ApiModelProperty(value = "order_no", name = "orderNo")
	private Long orderNo;
	/**
	 * menu_name
	 */
	@ApiModelProperty(value = "menu_name", name = "menuName")
	@NotBlank(message = "菜单名称不能为空")
	private String menuName;
	/**
	 * menu的样式
	 */
	@ApiModelProperty(value = "menu的样式", name = "menuIcon")
	private String menuIcon;

	/**
	 * url_address
	 */
	@ApiModelProperty(value = "url_address", name = "urlAddress")
	private String urlAddress;

	@ApiModelProperty(value = "是否可以删除（为0则不可删除，否则可删除）", name = "canDel")
	private Integer canDel;
	/**
	 * create_time
	 */
	@ApiModelProperty(value = "create_time", name = "createTime")
	private Date createTime;
	/**
	 * update_time
	 */
	@ApiModelProperty(value = "update_time", name = "updateTime")
	private Date updateTime;
	/**
	 * create_user_id
	 */
	@ApiModelProperty(value = "create_user_id", name = "createUserId")
	private Long createUserId;
	/**
	 * update_user_id
	 */
	@ApiModelProperty(value = "update_user_id", name = "updateUserId")
	private Long updateUserId;

	/**
	 * 前端树节点需要用这个属性
	 */
	@ApiModelProperty(value = "name", name = "name")
	@ExternalField
	private String name;

	/**
	 * 前端树节点使用
	 */
	@ApiModelProperty("是否为按钮 0-否 1-是")
	@ExternalField
	private Integer isButton;

	/**
	 * button在前端的的标识
	 */
	@ApiModelProperty(value = "module_tag_id", name = "moduleTagId")
	@ExternalField
	private String moduleTagId;

	@ApiModelProperty("角色id")
	@ExternalField
	private Long roleId;

	@ApiModelProperty("子菜单")
	@ExternalField
	private List<CmSysMenuEntity> children;

	@ApiModelProperty("拥有的按钮")
	@ExternalField
	private Map<Long, CmSysButtonEntity> buttons;

}
