package <#if isCloudModel>${cloudSysEngName}<#else>${packageName}</#if>.controller;

import java.util.List;
<#if !entityName??>
import java.util.Map;
</#if>

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import <#if isCloudModel>${cloudSysEngName}<#else>${packageName}</#if>.constant.ResultConstant;
import <#if isCloudModel>${cloudSysEngName}<#else>${packageName}</#if>.entity.CommonResult;
import <#if isCloudModel>${cloudSysEngName}<#else>${packageName}</#if>.service.IBaseService;

<#if !entityName??>
public class BaseController {

	private IBaseService service;

	public BaseController(IBaseService service) {
		this.service = service;
	}

	/**
	 * 查询
	 *
	 * @return
	 */
	@RequestMapping("/select")
	public CommonResult select(@RequestBody Map<String, Object> map) {
		return new CommonResult(ResultConstant.SUCCCSS_CODE, ResultConstant.SUCCESS_MSG, service.select(map));
	}

	/**
	 * 模糊查询
	 *
	 * @return
	 */
	@RequestMapping("/likeSelect")
	public CommonResult likeSelect(@RequestBody Map<String, Object> map) {
		return new CommonResult(ResultConstant.SUCCCSS_CODE, ResultConstant.SUCCESS_MSG, service.likeSelect(map));
	}

	/**
	 * 更新
	 *
	 * @return
	 */
	@RequestMapping("/update")
	public CommonResult update(@RequestBody Map<String, Object> map) {
		service.update(map);
		return new CommonResult(ResultConstant.SUCCCSS_CODE, ResultConstant.SUCCESS_MSG);
	}

	/**
	 * 添加
	 *
	 * @return
	 */
	@RequestMapping("/add")
	public CommonResult add(@RequestBody Map<String, Object> map) {
		service.add(map);
		return new CommonResult(ResultConstant.SUCCCSS_CODE, ResultConstant.SUCCESS_MSG);
	}

	/**
	 * 删除
	 *
	 * @return
	 */
	@RequestMapping("/delete")
	public CommonResult delete(@RequestBody Map<String, Object> map) {
		service.delete(map);
		return new CommonResult(ResultConstant.SUCCCSS_CODE, ResultConstant.SUCCESS_MSG);
	}

	/**
	 * 批量增加
	 *
	 * @return
	 */
	@RequestMapping("/batchAdd")
	public CommonResult batchAdd(@RequestBody List<Map<String, Object>> list) {
		service.batchAdd(list);
		return new CommonResult(ResultConstant.SUCCCSS_CODE, ResultConstant.SUCCESS_MSG);
	}

	/**
	 * 批量删除
	 *
	 * @return
	 */
	@RequestMapping("/batchDelete")
	public CommonResult batchDelete(@RequestBody List<Map<String, Object>> list) {
		service.batchDelete(list);
		return new CommonResult(ResultConstant.SUCCCSS_CODE, ResultConstant.SUCCESS_MSG);
	}

	/**
	 * 批量更新
	 *
	 * @return
	 */
	@RequestMapping("/batchUpdate")
	public CommonResult batchUpdate(@RequestBody List<Map<String, Object>> list) {
		service.batchUpdate(list);
		return new CommonResult(ResultConstant.SUCCCSS_CODE, ResultConstant.SUCCESS_MSG);
	}

}
<#else>
public class BaseController<E> {

	private IBaseService service;

	public BaseController(IBaseService service) {
		this.service = service;
	}

	/**
	 * 查询
	 *
	 * @return
	 */
	@RequestMapping("/select")
	public CommonResult select(@RequestBody E entity) {
		return new CommonResult(ResultConstant.SUCCCSS_CODE, ResultConstant.SUCCESS_MSG, service.select(entity));
	}

	/**
	 * 模糊查询
	 *
	 * @return
	 */
	@RequestMapping("/likeSelect")
	public CommonResult likeSelect(@RequestBody E entity) {
		return new CommonResult(ResultConstant.SUCCCSS_CODE, ResultConstant.SUCCESS_MSG, service.likeSelect(entity));
	}

	/**
	 * 更新
	 *
	 * @return
	 */
	@RequestMapping("/update")
	public CommonResult update(@RequestBody E entity) {
		service.update(entity);
		return new CommonResult(ResultConstant.SUCCCSS_CODE, ResultConstant.SUCCESS_MSG);
	}

	/**
	 * 添加
	 *
	 * @return
	 */
	@RequestMapping("/add")
	public CommonResult add(@RequestBody E entity) {
		service.add(entity);
		return new CommonResult(ResultConstant.SUCCCSS_CODE, ResultConstant.SUCCESS_MSG);
	}

	/**
	 * 删除
	 *
	 * @return
	 */
	@RequestMapping("/delete")
	public CommonResult delete(@RequestBody E entity) {
		service.delete(entity);
		return new CommonResult(ResultConstant.SUCCCSS_CODE, ResultConstant.SUCCESS_MSG);
	}

	/**
	 * 批量增加
	 *
	 * @return
	 */
	@RequestMapping("/batchAdd")
	public CommonResult batchAdd(@RequestBody List<E> list) {
		service.batchAdd(list);
		return new CommonResult(ResultConstant.SUCCCSS_CODE, ResultConstant.SUCCESS_MSG);
	}

	/**
	 * 批量删除
	 *
	 * @return
	 */
	@RequestMapping("/batchDelete")
	public CommonResult batchDelete(@RequestBody List<E> list) {
		service.batchDelete(list);
		return new CommonResult(ResultConstant.SUCCCSS_CODE, ResultConstant.SUCCESS_MSG);
	}

	/**
	 * 批量更新
	 *
	 * @return
	 */
	@RequestMapping("/batchUpdate")
	public CommonResult batchUpdate(@RequestBody List<E> list) {
		service.batchUpdate(list);
		return new CommonResult(ResultConstant.SUCCCSS_CODE, ResultConstant.SUCCESS_MSG);
	}

}
</#if>
